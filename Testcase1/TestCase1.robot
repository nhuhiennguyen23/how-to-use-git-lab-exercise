*** Settings ***
Library           ../PYTHON39/Scripts/KeyPyEx.py

*** Test Cases ***
TP1
    ${session}    OpenBrowser    https://demo.guru99.com/v4/
    clickElement    ${session}    //button[@id="details-button"]
    clickElement    ${session}    //a[@id="proceed-link"]
    clickElement    ${session}    //input[@name="uid"]
    clickElement    ${session}    //input[@name="password"]
    Inputvalue    ${session}    //input[@name="uid"]    mngr341579
    clear element text    ${session}    //input[@name="uid"]    attribute=none
    Inputvalue    ${session}    //input[@name="password"]\n    uzebYte
    clear element text    ${session}    //input[@name="password"]\n    attribute=none\n
    Clickelement    ${session}    //input[@name="btnReset"]
    Inputvalue    ${session}    //input[@name="uid"]    mngr341579
    Inputvalue    ${session}    //input[@name="password"]\n    uzebYte
    Clickelement    ${session}    //input[@name="btnLogin"]
    Location should be    https://demo.guru99.com/v4/manager/Managerhomepage.php
    close browser and end session \     ${session}

TP2
    ${session}    OpenBrowser    https://demo.guru99.com/v4/
    clickElement    ${session}    //button[@id="details-button"]
    clickElement    ${session}    //a[@id="proceed-link"]
    Login Form    //input[@name="uid"]    mngr341579    //input[@name="password"]    uzebYte
