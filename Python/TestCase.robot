*** Settings ***
Library           Selenium2Library

*** Test Cases ***
Test2
    [Documentation]    Exercise 1: Verify the Login Section
    ...    - Go to http://wwww.demo.guru99.com/v4
    ...    - Verify text 'User-ID must not be blank' and 'Password must not be blank' if userID or password field is empty
    ...    - Input userID and password
    ...    - Verify text 'User-ID must not be blank' and 'Password must not be blank' are disappeared
    ...    - Click Reset button
    ...    - Verify userID and password is cleared
    ...    - Input valid userID and valid password
    ...    - Click button login
    ...    - Verify login to home page successful
    ...    - Verify dynamic userID in home page
    ...    - Evidence screenshots are required for verification steps
    [Tags]    Test2
    Open Browser    https://demo.guru99.com/v4/    Chrome
    Click Button    //button[@id="details-button"]
    Click Link    //a[@id="proceed-link"]
    Input Text    //input[@type="text"]    mngr334878
    Clear Element Text    //input[@type="text"]
    Get Element Attribute    //input[@type="text"]    attribute=None
    Sleep    3s
    Input Text    //input[@type="password"]    EnUpApU
    Clear Element Text    //input[@type="password"]
    Get Element Attribute    //input[@type="password"]    attribute=None
    Sleep    3s
    Element Should Not Contain    //label[@id="message23"]    expected
    Input Text    //input[@type="text"]    mngr334878
    Input Password    //input[@type="password"]    EnUpApU
    Click Button    //input[@value="RESET"]
    ${value}    Get Text    //input[@type="text"]
    Should Be Equal    ${value}    ${EMPTY}
    ${value1}    Get Text    //input[@type="password"]
    Should Be Equal    ${value1}    ${EMPTY}
    Sleep    3s
    Input Text    //input[@type="text"]    mngr334878
    Input Password    //input[@type="password"]    EnUpApU
    Click button    //input[@value="LOGIN"]
    Location Should Be    https://demo.guru99.com/v4/manager/Managerhomepage.php

Test2a
    [Documentation]    Exercise 2: Verify add new customer
    ...    - Login home page
    ...    - select new customer
    ...    - Fill all fields
    ...    - submit form
    ...    - Verify add new customer's information correct with value input above
    ...    - Logout
    ...    - Verify login page displays
    ...    - Evidence screenshots are required for verification steps
    [Tags]    Test2a
    Open Browser    https://demo.guru99.com/v4/    Chrome
    Click Button    //button[@id="details-button"]
    Click Link    //a[@id="proceed-link"]
    Input Text    //input[@type="text"]    mngr334878
    Input Password    //input[@type="password"]    EnUpApU
    Click button    //input[@value="LOGIN"]
    Location Should Be    https://demo.guru99.com/v4/manager/Managerhomepage.php
    Comment    step2
    Click Element    //a[text()="New Customer"]
    Comment    Fill all fields
    Input Text    //input[@name="name"]    NguyenNhuHien
    Click button    //input[@value="f"]
    Input Text    //input[@name="dob"]    05/16/1993
    Input Text    //textarea[@name="addr"]    120 Le Duan
    Input Text    //input[@name="city"]    Da Nang City
    Input Text    //input[@name="state"]    Da Nang
    Input Text    //input[@name="pinno"]    123456
    Input Text    //input[@name="telephoneno"]    0905123789
    Input Text    //input[@name="emailid"]    nhuhiennguyen193@gmail.com
    Input Password    //input[@name="password"]    abc123
    Comment    Submit form
    Click button    //input[@value="Submit"]
    Comment    Log out
    Click Element    //a[text()="Continue"]
    Location Should Be    https://demo.guru99.com/v4/manager/Managerhomepage.php
    Click Element    //a[text()="Log out"]
    Location Should Be    https://demo.guru99.com/v4/index.php

TC03
    [Documentation]    Exercise 3: Verify sort order using xpath Axes method
    ...    - Go to URL: http://live.guru99.com
    ...    - Click on Mobile menu
    ...    - Verify sort by position
    ...    1. Sony Experia
    ...    2. IPHONE
    ...    3. SAMSUNG GALAXY
    ...    - Click sort by Name
    ...    - Verify sort by name
    ...    1. IPHONE
    ...    2. SAMSUNG GALAXY
    ...    3. Sony Experia
    ...    - Click sort by Price
    ...    - Verify sort by Price
    ...    1. Sony Experia
    ...    2. SAMSUNG GALAXY
    ...    3. IPHONE
    [Tags]    TC03
    Open Browser    http://live.guru99.com    Chrome
    Click Link    //a[@href="http://live.demoguru99.com/index.php/mobile.html"]
    Click element    //select[@onchange="setLocation(this.value)"]//child::option\n\n
    Click element    //select[@title='Sort By']/option[1]\n
    Sleep    5s
    Click element    //select[@title='Sort By']/option[2]
    Sleep    5s
    Click element    //select[@title='Sort By']/option[3]
